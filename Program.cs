﻿using System;
using FluentFTP;

class Program
{
    static void Main()
    {
        string serverAddress = "ftp://127.0.0.1";
        string username = "abhijasani";
        string password = "matrix";

        using (var ftp = new FtpClient(serverAddress))
        {
            ftp.Credentials = new System.Net.NetworkCredential(username, password);
            ftp.Connect();

            // Specify the local file path and the remote destination file path on the server
            string localFilePath = "/Users/abhijasani/Desktop/test/cat.mp4";
            string remoteFilePath = "/cat1.mp4";

            // Upload the file
            ftp.UploadFile(localFilePath, remoteFilePath, FtpRemoteExists.Overwrite, true);

            Console.WriteLine("File uploaded successfully.");

            ftp.Disconnect();
        }
    }
}
